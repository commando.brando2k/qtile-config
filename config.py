# -*- coding: utf-8 -*-

from libqtile.config import Key, Screen, Group, Drag, Click, Match
from libqtile.command import lazy
from libqtile import layout, bar, widget, hook

import platform
import re
import subprocess

#------------------------------------------------------------------------------
# Hooks

@hook.subscribe.startup_once
def autostart():
    subprocess.Popen(['dex', '-a', '-e awesome'])
    subprocess.Popen(['xrandr', '--output', 'DVI-1', '--right-of', 'DVI-0'])

@hook.subscribe.startup
def startup():
    subprocess.Popen(['feh', '--bg-fill', '/usr/share/archlinux/wallpaper/archlinux-simplyblack.png'])
    subprocess.Popen(['wmname', 'LG3D'])
    subprocess.Popen(['xsetroot', '-cursor_name', 'left_ptr'])

@hook.subscribe.client_focus
def dpms_toggle(client):
    if client.fullscreen:
        subprocess.Popen(['xset', 's', 'off'])
        subprocess.Popen(['xset', '-dpms'])
    else:
        subprocess.Popen(['xset', 's', 'on'])
        subprocess.Popen(['xset', '+dpms'])

@hook.subscribe.client_new
def float_dialog(client):
    if (client.window.get_wm_type() == 'dialog' or
        client.window.get_wm_role() == 'pop-up' or
        client.window.get_transient_for()):
        client.floating = True
        subprocess.Popen(['notify-send', 
                          'INFO', 
                          'Dialog window opened',
                          '-u', 'normal',
                          '-i', 'logo'])

@hook.subscribe.screen_change
def restart_on_xrandr(qtile, ev):
    subprocess.Popen(['notify-send', 
                      'INFO', 
                      'Qtile restarted because the screen changed',
                      '-u', 'normal',
                      '-i', 'logo'])
    qtile.cmd_restart()

@hook.subscribe.addgroup
def to_group(qtile, name):
    qtile.groupMap[name].cmd_toscreen()

def kbd_backlight_inc(qtile):
    return kbd_backlight(1)

def kbd_backlight_dec(qtile):
    return kbd_backlight(-1)

def kbd_backlight(val):
    _led = '/sys/class/leds/asus::kbd_backlight'
    _max = int(subprocess.Popen(['cat', _led + '/max_brightness'], stdout=subprocess.PIPE).communicate()[0])
    _prev = int(subprocess.Popen(['cat', _led + '/brightness'], stdout=subprocess.PIPE).communicate()[0])
    _cur = str(_prev)
    if (val > 0) and (_prev < _max):
        _cur = str(_prev + 1)
    elif (val < 0) and (_prev > 0):
        _cur = str(_prev - 1)

    p1 = subprocess.Popen(['echo', _cur], stdout=subprocess.PIPE)
    p2 = subprocess.Popen(['tee', _led + '/brightness'], stdin=p1.stdout)
    p1.stdout.close()

    return p2.returncode


#------------------------------------------------------------------------------

mod = "mod4"
keys = [
    # Switch between windows in current stack pane
    Key(
        [mod], "k",
        lazy.layout.down()
    ),
    Key(
        [mod], "j",
        lazy.layout.up()
    ),

    # Move windows up or down in current stack
    Key(
        [mod, "control"], "k",
        lazy.layout.shuffle_down()
    ),
    Key(
        [mod, "control"], "j",
        lazy.layout.shuffle_up()
    ),

    # Switch window focus to other pane(s) of stack
    Key(
        [mod], "space",
        lazy.layout.next()
    ),

    Key([mod, "control"], "h", lazy.to_screen(0)),
    Key([mod, "control"], "l", lazy.to_screen(1)),

    # Swap panes of split stack
    Key(
        [mod, "shift"], "space",
        lazy.layout.rotate()
    ),

    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key(
        [mod, "shift"], "Return",
        lazy.layout.toggle_split()
    ),
    Key([mod], "Return", lazy.spawn("termite")),

    Key([mod], "Tab", lazy.layout.next()),
    Key([mod, "shift"], "Tab", lazy.layout.previous()),
    Key([mod], "q", lazy.window.kill()),

    Key([mod], "f", lazy.window.toggle_fullscreen()),
    Key([mod, "shift"], "f", lazy.window.toggle_floating()),

    Key([mod], "i", lazy.layout.grow()),
    Key([mod], "m", lazy.layout.shrink()),
    Key([mod], "n", lazy.layout.normalize()),
    Key([mod], "o", lazy.layout.maximize()),

    Key([mod], "Up",
        lazy.spawn("amixer sset Master 5%+")),
    Key([mod], "Down",
        lazy.spawn("amixer sset Master 5%-")),
    Key([], "XF86AudioMute",
        lazy.spawn("amixer sset Master toggle")),
    Key([mod], "Right",
        lazy.spawn("/usr/bin/xbacklight -inc 5")),
    Key([mod], "Left",
        lazy.spawn("xbacklight -5")),
    Key([], "XF86KbdBrightnessUp",
        lazy.function(kbd_backlight_inc)),
    Key([], "XF86KbdBrightnessDown",
        lazy.function(kbd_backlight_dec)),

    Key([mod, "control"], "r", lazy.restart()),
    Key([mod, "control"], "q", lazy.shutdown()),
    Key([mod], "r", lazy.spawncmd()),
    Key([mod], "p", lazy.spawn("dmenu_run")),
]

groups = [Group(i) for i in "as"]

for i in groups:
    # mod1 + letter of group = switch to group
    keys.append(
        Key([mod], i.name, lazy.group[i.name].toscreen())
    )

    # mod1 + shift + letter of group = switch to & move focused window to group
    keys.append(
        Key([mod, "shift"], i.name, lazy.window.togroup(i.name))
    )

d_groups = {
    'www': {
        'group': {
            'layout': 'max',
            'matches': [Match(role=['browser'],
                              wm_class=['Chromium', 'chromium',
                                        'Chrome',
                                        'Google-chrome-stable',
                                        'Firefox'])],
            'exclusive': False,
            'persist': False,
            'init': False,
        },
        'key': 'w',
    },
    'term': {
        'group': {
            'layout': 'monadtall',
            'matches': [Match(wm_class=['termite',
                                        'Termite',
                                        'URxvt'])],
            'exclusive': False,
            'persist': False,
            'init': False,
        },
        'key': 't',
    },
    'dev': {
        'group': {
            'layout': 'monadtall',
            'matches': [Match(wm_class=['Eclipse',
                                        'gvim',
                                        'UE4Editor'])],
            'exclusive': False,
            'persist': False,
            'init': False,
        },
        'key': 'd',
    },
    'games': {
        'group': {
            'layout': 'monadtall',
            'matches': [Match(
                            title=[re.compile('Steam.*')],
                            wm_class=['Steam',
                                      'steam'],
                        )],
            'exclusive': False,
            'persist': False,
            'init': False,
        },
        'key': 'g',
    },
    'videos': {
        'group': {
            'layout': 'max',
            'matches': [Match(
                            title=[re.compile('Netflix.*')],
                            wm_class=['Kodi',
                                      'kodi'],
                        )],
            'exclusive': False,
            'persist': False,
            'init': False,
        },
        'key': 'v',
    },
}

for i in d_groups:
    k = d_groups[i].get('key')
    if k:
        # mod1 + letter of group = switch to group
        keys.append(
            Key([mod], k, lazy.group[i].toscreen())
        )

        # mod1 + shift + letter of group = switch to & move focused window to group
        keys.append(
            Key([mod, "shift"], k, lazy.window.togroup(i))
        )

    groups.append(Group(i, **d_groups[i]['group']))

layout_theme = dict(
    border_width=2,
    margin=3,
    border_focus='#1797d1',
    border_normal='#3f3f3f'
)

layouts = [
    layout.Max(**layout_theme),
    layout.Stack(num_stacks=2,**layout_theme),
    layout.Tile(**layout_theme),
    layout.MonadTall(**layout_theme),
    layout.Floating(**layout_theme)
]

theme = dict(
    bg_normal='#3f3f3f',
    fg_normal='#dcdccc', 
    fg_urgent='#cc9393', 
    fg_focus='#f0dfaf',
    border_focus='#1797d1',
    icon_path='/usr/share/icons/Faenza/status/22',
    theme_path='/usr/share/icons/Faenza/status/22',
)

battery_theme = dict(
    theme_path='/usr/share/icons/Faenza-Dark/status/22',
    custom_icons = {
        'battery-missing': 'battery-missing-symbolic.png',
        'battery-caution': 'battery-caution-symbolic.png',
        'battery-low': 'battery-low-symbolic.png',
        'battery-good': 'battery-good-symbolic.png',
        'battery-full': 'battery-full-symbolic.png',
        'battery-caution-charging': 'battery-caution-charging-symbolic.png',
        'battery-low-charging': 'battery-low-charging-symbolic.png',
        'battery-good-charging': 'battery-good-charging-symbolic.png',
        'battery-full-charging': 'battery-full-charging-symbolic.png',
        'battery-full-charged': 'battery-full-charged-symbolic.png',
    },
)

widget_defaults = dict(
    font='droid sans',
    fontsize=10,
    foreground=theme['fg_normal'],
    border_color=theme['border_focus'],
    fill_color=theme['border_focus'],
    padding=3,
)

num_screens = {
    'tha-beast': 2,
    'lw-laptop': 1,
    'badwolf': 1,
    'xephyr': 1,
}

system, node, release, version, machine, proc = platform.uname()

_top = [
    bar.Bar(
    [
        widget.GroupBox(urgent_alert_method="text", **widget_defaults),
        widget.Prompt(**widget_defaults),
        widget.TaskList(**widget_defaults),
        widget.TextBox("Updates:", **widget_defaults),
        widget.Pacman(unavailable=theme["fg_urgent"], **widget_defaults),
        widget.Volume(theme_path=theme["theme_path"], **widget_defaults),
        widget.Systray(**widget_defaults),
        widget.Notify(),
        widget.Clock(format='%Y-%m-%d %a %H:%M', **widget_defaults),
        widget.CurrentLayout(**widget_defaults),
    ], 24,
    ),

    bar.Bar(
    [
        widget.GroupBox(urgent_alert_method="text", **widget_defaults),
        widget.TaskList(**widget_defaults),
        widget.Clock(format='%Y-%m-%d %a %H:%M', **widget_defaults),
        widget.CurrentLayout(**widget_defaults),
    ], 24,
    ),
    
    bar.Bar(
    [
        widget.GroupBox(urgent_alert_method="text", **widget_defaults),
        widget.Prompt(**widget_defaults),
        widget.TaskList(**widget_defaults),
        widget.TextBox("Updates:", name="pacup"),
        widget.Pacman(unavailable=theme["fg_urgent"], **widget_defaults),
        widget.Volume(theme_path=theme['theme_path']),
        widget.Systray(),
        widget.BatteryIcon(**battery_theme),
        widget.Clock(format='%Y-%m-%d %a %H:%M', **widget_defaults),
        widget.Notify(),
        widget.CurrentLayout(**widget_defaults),
    ], 24,
    ),
]

_bot = [ 
    bar.Bar(
    [
        widget.Spacer(width=420),
        widget.TextBox(node + ' : ' + release + ' on ' + machine),
        widget.Sep(**widget_defaults),
        widget.TextBox("Core1:", **widget_defaults),
        widget.CPUGraph(core=0,
                        type='linefill',
                        line_width=2,
                        width=48,
                        **widget_defaults),
        widget.TextBox("Core2:", **widget_defaults),
        widget.CPUGraph(core=1,
                        type='linefill',
                        line_width=2,
                        width=48,
                        **widget_defaults),
        widget.TextBox("Core3:", **widget_defaults),
        widget.CPUGraph(core=2,
                        type='linefill',
                        line_width=2,
                        width=48,
                        **widget_defaults),
        widget.TextBox("Core4:", **widget_defaults),
        widget.CPUGraph(core=3,
                        type='linefill',
                        line_width=2,
                        width=48,
                        **widget_defaults),
        widget.Sep(**widget_defaults),
        widget.TextBox("NET:", **widget_defaults),
        widget.Net(interface='enp3s0', **widget_defaults),
        widget.Sep(**widget_defaults),
    ], 24,
    ),
]

if num_screens[node] == 2:
    screens = [
        Screen(
            top = _top[0],
            bottom = bar.Gap(12)
            #bottom = _bot[0]
        ),
        Screen(
            top = _top[1],
            bottom = bar.Gap(12)
        ),
    ]

else:
    screens = [
        Screen(
            top = _top[2],
            bottom = bar.Gap(12)
        ),
    ]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
        start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
        start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []
main = None
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating()
auto_fullscreen = True
wmname = "qtile"
